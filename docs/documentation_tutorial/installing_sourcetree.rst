=======================
 Installing SourceTree
=======================

SourceTree is a user friendly interface for the Version Control tool Git.

It's main benefits for local development are:

	* Backup your commited changes to prevent accidental destruction of your work
	* Allows for collaboration between multiple individuals in a scalable and relatively painless manor
	* Allows for offline editing as opposed to a tool like Google Docs

Downloading SourceTree
----------------------

To download SourceTree go to the link below:

	http://www.sourcetreeapp.com/

.. Note:: Sourcetree should install without any errors, however if you are having trouble getting it to install, please contact S3.