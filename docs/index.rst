.. 8th Special Operation Force - Training Resources documentation master file, created by
   sphinx-quickstart on Tue Apr  8 02:21:06 2014.

Welcome to 8th Special Operation Force - Training Resources's
==============================================================

:Organisation: 8th Special Operation Force
:Contact: relations@8th-sof.com
:Copyright: This work is placed in the public domain in the hope that all of the ARMA community may benefit. We would appreciate the retention of the dedication and a link to our unit, although neither is compulsory. 
:Dedication: To those who live and die for their country.

Introduction
------------

This is the central resource for all training documentation that belongs to the 8th SOF Unit.

We are an ARMA 3 MILSIM unit orientated around innovative gameplay and collaborative training. This is our central resource for ARMA related tactics, and it is fully free and open source!

Contributing
------------

Typos, technical errors and editorial oversights can be fixed via a pull request on our BitBucket repository.

:Repository URL: https://bitbucket.org/8th-sof/training-standards-and-manuals-repository

While the main branch you read here is maintained only by members of the 8th SOF, you may fork this on BitBucket and create a set of documentation unique to your own units!

.. note:: Please refrain from word for word copying of field manuals. While alot of the information will be similar, we would prefer to keep this document our own and use methods that we know work well in ARMA, not just real life.

Contents
----------

.. toctree::
   :maxdepth: 1

   basic_training/landing_page
   documentation_tutorial/landing_page


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

