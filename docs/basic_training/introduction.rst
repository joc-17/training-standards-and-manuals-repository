==============
 Introduction
==============

This is the first manual you will receive upon joining the 8th SOF. It contains all of the basic information that we think is required to play ARMA at a reasonable level of competency. Many of the more advanced combat orientated actions will be introduced in the next manual "Advanced Infantry Training" and subsequent courses that you may take with us.

.. Hint:: Make sure to raise any questions you have with your training instructor. He will be able to help you get the most out of this manual!
