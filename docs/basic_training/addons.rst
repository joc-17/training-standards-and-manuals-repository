======================
Installing our Addons
======================

We use a wide variety of mods in order to make ARMA a more authentic MILSIM experience.

Your training instructor will guide you through the process of installing our modpack using the ARMA3Sync Client in your "Introduction to the 8th SOF" session. 

A video will be posted here in the future.
